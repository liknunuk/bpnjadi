<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Admin extends Controller
{
  // Jebakan Login
  private function cekAdmin()
  {
    if (!$_SESSION['admin'] || $_SESSION['admin'] == false) {
      header("Location:" . BASEURL . "Admin/login");
    }
  }

  // Managemen User
  public function index($pn = 1)
  {
    $this->cekAdmin();
    // daftar user
    // form user sebagai modal
    $data = [
      'users' => $this->model('Model_user')->tampil(1)
    ];
    $this->view('template/header');
    $this->view('admin/adminAppBar');
    $this->view('admin/index', $data);
    $this->view('template/footer');
  }

  public function ngarahUser($data)
  {
    // pencarian data user
  }

  public function genaehUser($id)
  {
    // data detil user
  }

  public function tambahUser()
  {
    // tambah pengguna
    if ($this->model('Model_user')->tambah($_POST) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'Data pengguna', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'Data pengguna', 'warning');
    }
    header("Location:" . BASEURL . "Admin");
  }
  public function ngubahUser()
  {
    // ubah pengguna
    if ($this->model('Model_user')->ngubah($_POST) > 0) {
      Alert::setAlert('berhasil dimutakhirkan', 'Data pengguna', 'success');
    } else {
      Alert::setAlert('gagal dimutakhirkan', 'Data pengguna', 'warning');
    }
    header("Location:" . BASEURL . "Admin");
  }
  public function sampahUser()
  {
    echo $this->model('Model_user')->sampah($_POST) > 0 ? "1" : "0";
  }
  public function resetPassword()
  {
    if ($this->model('Model_user')->userChPass($_POST) > 0) {
      Alert::setAlert('berhasil diubah', 'Password User', 'success');
    } else {
      Alert::setAlert('gagal diubah', 'Password User', 'warning');
    }
    header("Location:" . BASEURL . "Admin");
  }

  public function login()
  {
    $data['title'] = "admin";
    $this->view('template/header', $data);
    $this->view('admin/adminAppBar');
    $this->view('admin/login');
    $this->view('template/footer');
  }

  public function auth()
  {
    $user = $this->model('Model_user')->auth($_POST);
    $eksis = COUNT($user);
    if ($eksis == 5) {
      $_SESSION['admin'] = true;
      $_SESSION['fullName'] = $user['fullName'];
      header("Location:" . BASEURL . "Admin");
    } else {
      header("Location:" . BASEURL . "Admin/login");
    }
  }

  public function deauth()
  {
    session_unset();
    session_destroy();
    header("Location:" . BASEURL . "Admin/login");
  }
}
