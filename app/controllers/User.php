<?php
// sesuaikan nama kelas, tetap extends ke Controller
class User extends Controller
{
  // method default
  //   Login Notaris
  public function index()
  {
    // Form Pencarian Berkas Sertifikat
    $data['gol'] = "Notaris";
    $this->view('template/header');
    $this->view('home/homeAppBar');
    $this->view('user/index', $data);
    $this->view('template/footer');
  }

  // Login Loket
  public function loket()
  {
    // Form Pencarian Berkas Sertifikat
    $data['gol'] = "Loket";
    $this->view('template/header');
    $this->view('home/homeAppBar');
    $this->view('user/index', $data);
    $this->view('template/footer');
  }

  public function notaris()
  {
    // Form Pencarian Berkas Sertifikat
    $data['gol'] = "Notaris";
    $this->view('template/header');
    $this->view('home/homeAppBar');
    $this->view('user/notaris', $data);
    $this->view('template/footer');
  }

  public function hhp()
  {
    // Form Pencarian Berkas Sertifikat
    $data['gol'] = "Hhp";
    $this->view('template/header');
    $this->view('home/homeAppBar');
    $this->view('user/index', $data);
    $this->view('template/footer');
  }

  public function userAuth()
  {
    $userInfo = $this->model('Model_user')->auth($_POST);
    $_SESSION['userID'] = $userInfo['userID'];
    $_SESSION['fullName'] = $userInfo['fullName'];
    $_SESSION['hakAkses'] = $userInfo['hakAkses'];
    header("Location: " . BASEURL . $userInfo['hakAkses']);
  }

  public function notarisAuth()
  {
    $userInfo = $this->model('Model_user')->notarisAuth($_POST);
    if (COUNT($userInfo) > 1) {
      $_SESSION['idppatk'] = $userInfo['nikPengaju'];
      $_SESSION['namaLengkap'] = $userInfo['namaLengkap'];
      $_SESSION['alamat'] = $userInfo['alamat'];
      $_SESSION['nomorTelepon'] = $userInfo['nomorTelepon'];
      header("Location:" . BASEURL . "Notaris");
    } else {
      header("Location:" . BASEURL . "User/notaris");
    }
  }

  public function klilan($ha)
  {
    session_unset();
    session_destroy();
    $url = rtrim(BASEURL . "User/" . $ha . '/');
    // echo $url;
    header("Location: " . $url);
  }
}
