<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Hhp extends Controller
{
  // method default
  public function index()
  {
    // Data Berkas Jadi Bulan Berjalan
    $bulan = date('Y-m');
    $tahun = date('Y');
    $data['jadibulan'] = $this->model('Model_berkas')->jadibulan($bulan);
    $data['jaditahun'] = $this->model('Model_berkas')->jaditahun($tahun);
    $data['ambilbulan'] = $this->model('Model_berkas')->ambilbulan($bulan);
    $data['ambiltahun'] = $this->model('Model_berkas')->ambiltahun($tahun);
    $data['sisabulan'] = $this->model('Model_berkas')->sisabulan($bulan);
    $data['sisatahun'] = $this->model('Model_berkas')->sisatahun($tahun);
    $this->view('template/header');
    $this->view('hhp/hhpAppBar');
    $this->view('hhp/index', $data);
    $this->view('template/footer');
  }

  public function berkasNumpuk()
  {
    // Data berkas jadi, belum diambil, bulan berjalan.
  }

  public function berkasKeluar()
  {
  }

  public function berkas($nomorBerkas)
  {
    $data = [
      'berkas' => $this->model('Model_berkas')->detail($nomorBerkas)
    ];
    $data['taker'] = $data['berkas']['statusBerkas'] == "0" ? $this->model('Model_pemohon')->detail($data['berkas']['diambilOleh']) : NULL;
    $this->view('template/header');
    $this->view('hhp/hhpAppBar');
    $this->view('hhp/berkas', $data);
    $this->view('template/footer');
  }

  public function cetakBerkas($nomorBerkas)
  {
    $data = [
      'berkas' => $this->model('Model_berkas')->detail($nomorBerkas)
    ];
    $this->view('hhp/cetakBerkas', $data);
  }

  public function statistikPeriodik($m, $s)
  {
    $data = $this->model('Model_berkas')->statistikPeriodik($m, $s);
    echo json_encode($data, JSON_PRETTY_PRINT);
  }

  public function berkasPeriodik($m, $s)
  {
    $data = $this->model('Model_berkas')->berkasPeriodik($m, $s);
    echo json_encode($data, JSON_PRETTY_PRINT);
  }

  public function cetakRentang($m, $s)
  {
    $data['berkas'] = $this->model('Model_berkas')->berkasPeriodik($m, $s);
    $this->view('hhp/cetakRentang', $data);
  }
}
