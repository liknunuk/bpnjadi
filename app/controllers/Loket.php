<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Loket extends Controller
{

  // check login session
  public function __construct()
  {
    if (!isset($_SESSION) || $_SESSION['hakAkses'] != 'Loket') header("Location:" . BASEURL . "User/loket");
  }

  // method default
  // Management Berkas Jadi
  public function index($pn = 1)
  {
    $data['blmAmbil'] = $this->model('Model_berkas')->tampilJadi($pn);
    $data['sdhAmbil'] = $this->model('Model_berkas')->tampilAmbil($pn);
    // Daftar Berjas Sertifikat Jadi
    $this->view('template/header');
    $this->view('loket/loketAppBar');
    $this->view('loket/index', $data);
    $this->view('template/footer');
  }

  public function frBerkas($nomorBerkas = "")
  {
    // Kolom Berkas Jadi : kegiatan , nomorBerkas , tahunBerkas , nikPemilik, namaPemilik, jenisHak , nomorHak , tanggalJadi, desa
    if ($nomorBerkas == "") {
      $data['berkas']['kegiatan'] = NULL;
      $data['berkas']['nomorBerkas'] = NULL;
      $data['berkas']['tahunBerkas'] = date('Y');
      $data['berkas']['namaPemilik'] = NULL;
      $data['berkas']['jenisHak'] = NULL;
      $data['berkas']['nomorHak'] = NULL;
      $data['berkas']['tanggalJadi'] = date('Y-m-d');
      $data['berkas']['desa'] = NULL;
      $data['wilayah'] = NULL;
      $data['modus'] = "enggal";
    } else {
      $data['berkas'] = $this->model('Model_berkas')->detail($nomorBerkas);
      $data['wilayah'] = $this->model('Model_berkas')->wilayah($data['berkas']['desa']);
      $data['modus'] = "gantos";
    }
    // formulir berjas jadi
    $this->view('template/header');
    $this->view('loket/loketAppBar');
    $this->view('loket/frBerkas', $data);
    $this->view('template/footer');
  }

  public function berkasTambah()
  {
    // tambah berkas jadi
    // print_r($_POST);
    if ($_POST['modus'] == 'enggal') {
      if ($this->model('Model_berkas')->tambah($_POST) > 0) {
        Alert::setAlert('berhasil ditambahkan', 'Data sertipikat', 'success');
      } else {
        Alert::setAlert('gagal ditambahkan', 'Data sertipikat', 'danger');
      }
      header("Location:" . BASEURL . "Loket");
    } else {
      $this->berkasDiubah($_POST);
    }
  }

  public function berkasDiubah()
  {
    // update data berkas
    if ($this->model('Model_berkas')->ngubah($_POST) > 0) {
      Alert::setAlert('berhasil dimutakhirkan', 'Data sertipikat', 'success');
    } else {
      Alert::setAlert('gagal dimutakhirkan', 'Data sertipikat', 'danger');
    }
    header("Location:" . BASEURL . "Loket");
  }

  public function berkasDetail($nb)
  {
    // $data['berkas'] = $this->model('Model_berkas')->detail($nb);
    $idb = explode("-", $nb);
    if (COUNT($idb) <= 2) {
      $db = $nb;
    } else {
      $db = str_replace("-", "/", $nb);
    }
    $data['berkas'] = $this->model('Model_berkas')->cariberkas($db);
    if ($data['berkas']['diambilOleh'] != NULL) {
      $data['taker'] = $this->model('Model_pemohon')->detail($data['berkas']['diambilOleh']);
    } else {
      $data['taker']['namaLengkap'] = '';
      $data['taker']['golongan'] = '';
      $data['taker']['alamat'] = '';
    }
    $this->view('template/header');
    $this->view('loket/loketAppBar');
    $this->view('loket/dtBerkas', $data);
    $this->view('template/footer');
  }

  public function berkasCetak($nb)
  {
    $data['berkas'] = $this->model('Model_berkas')->detail($nb);

    // $data['berkas'] = $this->model('Model_berkas')->cariberkas($nb);
    if ($data['berkas']['diambilOleh'] != NULL) {
      $data['taker'] = $this->model('Model_pemohon')->detail($data['berkas']['diambilOleh']);
    } else {
      $data['taker']['namaLengkap'] = '';
      $data['taker']['golongan'] = '';
      $data['taker']['alamat'] = '';
    }

    $this->view('loket/ctBerkas', $data);
  }

  public function berkasSampah()
  {
    // hapus data berkas
    if ($this->model('Model_berkas')->sampah($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
  }

  // Manajemen Pengaju / Kustomer
  public function kustomer($flow = 'bb', $nb = 0)
  {
    // form pencarian customer. Pengaju / Pengambil
    // Form kustomer menadi Modal
    $data['flow'] = $flow;
    $data['berkas'] = $nb;
    $_SESSION['stpkNomor'] = $nb;
    $this->view('template/header');
    $this->view('loket/loketAppBar');
    $this->view('loket/kustomer', $data);
    $this->view('template/footer');
  }

  public function pengajuNgarah()
  {
    // pencarian data pengaju / kustomer
    $data = $this->model('Model_pemohon')->detail($_POST['nik']);
    if ($data > 0) {
      $_SESSION['stpkNIK'] = $_POST['nik'];
      $result = ['hasil' => 'Ditemukan', 'data' => $data];
    } else {
      $result = ['hasil' => 'Tidak Ditemukan', 'data' => NULL];
    }

    echo json_encode($result);
  }

  public function cekPemohon()
  {
    $eksis = $this->model('Model_pemohon')->cekPemohon($_POST);
    if ($eksis >= 1) {
      echo "Data Pemohon Ditemukan";
    } else {
      echo $eksis;
    }
  }

  public function pengajuTambah()
  {
    // tambah data pengaju / kustomer
    if ($this->model('Model_pemohon')->tambah($_POST) > 0) {
      $_SESSION['stpkNIK'] = $_POST['nikPengaju'];
      header("Location:" . BASEURL . "Loket/frAmbil");
    } else {
      header("Location:" . BASEURL . "Loket/kustomer/ab");
    }
  }

  public function pemohonTambah()
  {
    // tambah data pengaju / kustomer
    if ($this->model('Model_pemohon')->tambah($_POST) > 0) {
      $_SESSION['stpkNIK'] = $_POST['nikPengaju'];
      header("Location:" . BASEURL . "Loket/frBerkas");
    } else {
      header("Location:" . BASEURL . "Loket/frBerkas");
    }
  }

  public function notarisTambah()
  {
    if ($_POST['mode'] == 'ubah') {
      $this->notarisBrubah($_POST);
    }
    // tambah data pengaju / kustomer
    if ($this->model('Model_pemohon')->tambahNotaris($_POST) > 0) {
      header("Location:" . BASEURL . "Loket/notaris");
    } else {
      header("Location:" . BASEURL . "Loket/notaris");
    }
  }

  private function notarisBrubah($data)
  {
    if ($this->model('Model_pemohon')->ngubah($data) > 0) {
      header("Location:" . BASEURL . "Loket/notaris");
    } else {
      header("Location:" . BASEURL . "Loket/notaris");
    }
  }

  public function pengajuDiubah($data)
  {
    // update data pengaju
  }

  public function pengajuSampah()
  {
    // hapus data pengaju
    if ($this->model('Model_pemohon')->sampah($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
  }

  // Management Pengambilan
  public function frAmbil()
  {
    // Formulir pengambilan berkas
    $data['pemohon'] = $this->model('Model_pemohon')->detail($_SESSION['stpkNIK']);
    $this->view('template/header');
    $this->view('loket/loketAppBar');
    $this->view('loket/frAmberkas', $data);
    $this->view('template/footer');
  }

  public function ambilNambah()
  {
    // tambah data pengambilan
    // update data berkasjadi
    if ($this->model('Model_berkas')->diambil($_POST) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'Data pengambilan sertipikat', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'Data pengambilan sertipikat', 'danger');
    }
    unset($_SESSION['stpkNomor']);
    unset($_SESSION['stpkNIK']);
    header("Location:" . BASEURL . "Loket");
  }

  public function ambilNgubah($id)
  {
    // Update data pengambilan pada berkas jadi
  }

  // Notaris Mitra
  public function notaris($pn = 1)
  {
    $data = [
      'title' => 'Notaris Mitra',
      'ppatk' => $this->model('Model_pemohon')->daftarNotaris($pn)
    ];

    $this->view('template/header');
    $this->view('loket/loketAppBar');
    $this->view('loket/notaris', $data);
    $this->view('template/footer');
  }

  public function dataNotaris($nik)
  {
    $notaris = $this->model('Model_pemohon')->detail($nik);
    echo json_encode($notaris, JSON_PRETTY_PRINT);
  }

  public function desa($id)
  {
    // query data desa
    $data['desa'] = $this->model('Model_desa')->kodeDesa($id);
    echo json_encode($data['desa'], JSON_PRETTY_PRINT);
  }
}
