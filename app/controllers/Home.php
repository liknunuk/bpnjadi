<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  // Formulir pencarian berkas by nomor berkas
  public function index()
  {
    // Form Pencarian Berkas Sertifikat
    $this->view('template/header');
    $this->view('home/homeAppBar');
    $this->view('home/index');
    $this->view('template/footer');
  }

  public function detil($nomorBerkas){
    $data = $this->model('Model_berkas')->cekStatus($nomorBerkas);
    
    echo json_encode( $data,JSON_PRETTY_PRINT);
  }
}