<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Notaris extends Controller
{

    public function __construct()
    {
        if (!isset($_SESSION['idppatk'])) {
            header("Location:" . BASEURL . "User/notaris");
        }
    }

    // method defaul
    public function index()
    {
        // halaman utama notaris
        $data['berkas'] = $this->model('Model_berkas')->berkasNotaris($_SESSION['idppatk']);
        $this->view('template/header');
        $this->view('home/homeAppBar');
        $this->view('notaris/index', $data);
        $this->view('template/footer');
    }

    public function gandi()
    {
        // halaman utama notaris
        $this->view('template/header');
        $this->view('home/homeAppBar');
        $this->view('notaris/gandi');
        $this->view('template/footer');
    }

    public function cetak()
    {
        $data['berkas'] = $this->model('Model_berkas')->berkasNotaris($_SESSION['idppatk']);
        $this->view('notaris/cetak', $data);
    }

    public function passcha()
    {
        if ($this->model('Model_pemohon')->passcha($_POST) > 0) {
            Alert::setAlert("berhasil diubah", "Kata sasndi", "success");
        } else {
            Alert::setAlert("gagal diubah", "Kata sasndi", "danger");
        }

        header("Location:" . BASEURL . "Notaris");
    }

    public function berkas($nomorBerkas)
    {
        $data['berkas'] = $this->model('Model_berkas')->detail($nomorBerkas);
        $this->view('template/header');
        $this->view('home/homeAppBar');
        $this->view('notaris/berkas', $data);
        $this->view('template/footer');
    }

    public function ceberk($nomorBerkas)
    {
        $data['berkas'] = $this->model('Model_berkas')->detail($nomorBerkas);
        $this->view('notaris/ceberk', $data);
    }
}
