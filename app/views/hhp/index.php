<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center py-3 page-title">LAPORAN PENGAMBILAN SERTIPIKAT</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 mx-auto">
            <table class="table table-sm table-bpn table-hhp">
                <thead>
                    <tr>
                        <th>KETERANGAN</th>
                        <th>DATA BULAN BERJALAN</th>
                        <th>DATA TAHUN BERJALAN</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>SERTIPIKAT JADI</td>
                        <td class='text-right pr-3'><?= $data['jadibulan']['jumlah']; ?> Berkas</td>
                        <td class='text-right pr-3'><?= $data['jaditahun']['jumlah']; ?> Berkas</td>
                    </tr>
                    <tr>
                        <td>SERTIPIKAT DIAMBIL</td>
                        <td class='text-right pr-3'><?= $data['ambilbulan']['jumlah']; ?> Berkas</td>
                        <td class='text-right pr-3'><?= $data['ambiltahun']['jumlah']; ?> Berkas</td>
                    </tr>
                    <tr>
                        <td>SERTIPIKAT TERSISA</td>
                        <td class='text-right pr-3'><?= $data['sisabulan']['jumlah']; ?> Berkas</td>
                        <td class='text-right pr-3'><?= $data['sisatahun']['jumlah']; ?> Berkas</td>
                    </tr>
                </tbody>
            </table>

            <div class="card">
                <div class="card-header">
                    <h4 class="text-center py-3 page-title">PENGAMBILAN SERTIPIKAT MENURUT RENTANG WAKTU</h4>
                </div>
                <div class="card-body row">
                    <div class="col-md-4">
                        <label for="tgMulai">Sejak Tanggal</label>
                        <input type="date" class="form-control" id="tgMulai">
                    </div>
                    <div class="col-md-4">
                        <label for="tgAkhir">Hingga Tanggal</label>
                        <input type="date" class="form-control" id="tgAkhir">
                    </div>
                    <div class="col-md-2">
                        <button id="rekapPeriod" class="btn btn-success">
                            <h4>TAMPILKAN DATA</h4>
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button id="cetakPeriod" class="btn btn-success">
                            <h4>CETAK DATA</h4>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bpn table-hhp">
                        <thead>
                            <tr class='bg-dark text-light'>
                                <th class="text-center">Berkas Jadi</th>
                                <th class="text-center">Berkas Diambil</th>
                                <th class="text-center">Berkas Tersisa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td id="perJadi" class='text-center'></td>
                                <td id="perAmbil" class='text-center'></td>
                                <td id="perSisa" class='text-center'></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-sm table-bpn">
                <thead>
                    <tr>
                        <th rowspan="2">Kegiatan</th>
                        <th rowspan="2">Nomor Berkas</th>
                        <th rowspan="2">Nama Pemilik</th>
                        <th rowspan="2">Desa, Kecamatan</th>
                        <th colspan="2">Hak</th>
                        <th rowspan="2">Tanggal<br />Penyerahan Loket</th>
                        <th rowspan="2">Tanggal<br />Pengambilan</th>
                        <th rowspan="2">
                            <i class="fas fa-gear"></i>
                        </th>
                    </tr>
                    <tr>
                        <th>Jenis</th>
                        <th>Nomor</th>
                    </tr>
                </thead>
                <tbody id="berkas">
                </tbody>
            </table>
        </div>
    </div>

</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $("#rekapPeriod").click(function() {
        let m, s;
        m = $("#tgMulai").val();
        s = $("#tgAkhir").val();
        $.ajax({
            dataType: 'json',
            url: `<?= BASEURL; ?>Hhp/statistikPeriodik/${m}/${s}`,
            success: function(res) {
                $("#perJadi").text("");
                $("#perAmbil").text("");
                $("#perSisa").text("");
                $("#perJadi").text(res.jadi);
                $("#perAmbil").text(res.ambil);
                $("#perSisa").text(res.sisa);
            }
        })

        $.ajax({
            dataType: 'json',
            url: `<?= BASEURL; ?>Hhp/berkasPeriodik/${m}/${s}`,
            success: function(res) {
                console.log(res);
                $("#berkas tr").remove();
                $.each(res, function(i, data) {
                    $("#berkas").append(`
                    <tr>
                    <td>${data.kegiatan}</td>
                    <td>${data.nomorBerkas}</td>
                    <td>${data.namaPemilik}</td>
                    <td>${data.namadesa}<br/>${data.namakecamatan}</td>
                    <td>${data.jenisHak}</td>
                    <td>${data.nomorHak}</td>
                    <td>${data.tanggalJadi}</td>
                    <td>${data.diambilTanggal}</td>
                    <td>
                    <a href="<?= BASEURL; ?>Hhp/berkas/${data.nomorBerkas}">detil</a>
                    </td>
                    `)
                });
            }
        })

    })

    $("#cetakPeriod").click(function() {
        let m, s;
        m = $("#tgMulai").val();
        s = $("#tgAkhir").val();
        window.open("<?= BASEURL; ?>hhp/cetakRentang/" + m + "/" + s, "dataRentang", "width=1000,height=1000,top=0,left=0");
    })
</script>