<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body onload="window.print()">
    <center>
        <h4>Data Berkas Jadi dan Pengambilan Berkas</h4>
        <h4>Periode</h4>
    </center>
    <table align="center" width="100%" border="1" cellspacing="0">
        <thead>
            <tr>
                <th rowspan="2">Kegiatan</th>
                <th rowspan="2">Nomor Berkas</th>
                <th rowspan="2">Nama Pemilik</th>
                <th rowspan="2">Desa, Kecamatan</th>
                <th colspan="2">Hak</th>
                <th rowspan="2">Tanggal<br />Penyerahan Loket</th>
                <th rowspan="2">Tanggal<br />Pengambilan</th>
            </tr>
            <tr>
                <th>Jenis</th>
                <th>Nomor</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['berkas'] as $berkas) : ?>
                <tr>
                    <td><?= $berkas['kegiatan']; ?></td>
                    <td><?= $berkas['nomorBerkas']; ?></td>
                    <td><?= $berkas['namaPemilik']; ?></td>
                    <td><?= $berkas['namadesa'] . ',' . $berkas['kecamatan']; ?></td>
                    <td><?= $berkas['jenisHak']; ?></td>
                    <td><?= $berkas['nomorHak']; ?></td>
                    <td><?= $berkas['tanggalJadi']; ?></td>
                    <td><?= $berkas['diambilTanggal']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>

</html>