<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">DATA LENGKAP BERKAS SERTIPIKAT</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mx-auto table-responsive">
            <table class="table table-sm table-striped">
                <tbody>
                    <tr>
                        <td>Kegiatan</td>
                        <td><?= $data['berkas']['kegiatan']; ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Berkas</td>
                        <td><?= $data['berkas']['nomorBerkas']; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Pemilik</td>
                        <td><?= $data['berkas']['namaPemilik']; ?></td>
                    </tr>
                    <tr>
                        <td>Lokasi</td>
                        <td>Desa/Kelurahan <?= $data['berkas']['namaDesa']; ?>, Kecamatan <?= $data['berkas']['kecamatan']; ?></td>
                    </tr>
                    <tr style="font-family:monospace;">
                        <td>Hak</td>
                        <td>Jenis: <?= $data['berkas']['jenisHak']; ?><br />Nomor: <?= $data['berkas']['nomorHak']; ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Penyerahan Loket</td>
                        <td><?= $data['berkas']['tanggalJadi']; ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Tanggungan</td>
                        <td><?= $data['berkas']['nomorTanggungan']; ?></td>
                    </tr>
                    <tr>
                        <td>Keterangan Lain</td>
                        <td><?= $data['berkas']['keterangan']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Pengambilan</td>
                    </tr>
                    <tr>
                        <td>Status Pengambilan</td>
                        <td>
                            <?php
                            function stambil($stpa)
                            {
                                echo $stpa == "1" ? "Belum Diambil" : "Sudah Diambil";
                            }
                            echo $data['berkas']['statusBerkas'] . " - ";
                            stambil($data['berkas']['statusBerkas']);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal Pengambilan</td>
                        <td><?= $data['berkas']['diambilTanggal']; ?></td>
                    </tr>
                    <tr>
                        <td>Diambil Oleh</td>
                        <td>
                            <?= $data['berkas']['diambilOleh']; ?><br />
                            <?= $data['taker']['namaLengkap']; ?><br />
                            <?= $data['taker']['golongan']; ?><br />
                            <?= $data['taker']['nomorTelepon']; ?><br />
                        </td>
                    </tr>
                    <tr>
                        <td>Surat Kuasa</td>
                        <td>
                            <?php if ($data['berkas']['suratKuasa'] != "") : ?>
                                <a href="<?= $data['berkas']['suratKuasa']; ?>" target="_blank">Buka Surat Kuasa</a>
                            <?php else : ?>
                                Tidak Ada Surat Kuasa
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <a href="<?= BASEURL; ?>Hhp">Kembali</a>
                            &nbsp;
                            <a href="javascript:void(0)" onClick=cetak("<?= $data['berkas']['nomorBerkas']; ?>")>Cetak</a>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    function cetak(nomorBerkas) {
        window.open("<?= BASEURL; ?>Hhp/cetakBerkas/" + nomorBerkas, "berkas", "width=800,height=1000,left=200,top=300");
    }
</script>