<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body onload="window.print()">
    <center>
        <h4>Data Berkas Nomor : <?= $data['berkas']['nomorBerkas']; ?></h4>
    </center>
    <table align="center" size="600" border="1" cellspacing="0">
        <tbody>
            <tr>
                <td>Kegiatan</td>
                <td><?= $data['berkas']['kegiatan']; ?></td>
            </tr>
            <tr>
                <td>Nomor Berkas</td>
                <td><?= $data['berkas']['nomorBerkas']; ?></td>
            </tr>
            <tr>
                <td>Nama Pemilik</td>
                <td><?= $data['berkas']['namaPemilik']; ?></td>
            </tr>
            <tr>
                <td>Lokasi</td>
                <td>Desa/Kelurahan <?= $data['berkas']['namaDesa']; ?>, Kecamatan <?= $data['berkas']['kecamatan']; ?></td>
            </tr>
            <tr style="font-family:monospace;">
                <td>Hak</td>
                <td>Jenis: <?= $data['berkas']['jenisHak']; ?><br />Nomor: <?= $data['berkas']['nomorHak']; ?></td>
            </tr>
            <tr>
                <td>Tanggal Penyerahan Loket</td>
                <td><?= $data['berkas']['tanggalJadi']; ?></td>
            </tr>
            <tr>
                <td>Nomor Tanggungan</td>
                <td><?= $data['berkas']['nomorTanggungan']; ?></td>
            </tr>
            <tr>
                <td>Keterangan Lain</td>
                <td><?= $data['berkas']['keterangan']; ?></td>
            </tr>
            <tr>
                <td colspan="2">Pengambilan</td>
            </tr>
            <tr>
                <td>Status Pengambilan</td>
                <td>
                    <?php
                    function stambil($stpa)
                    {
                        echo $stpa == "1" ? "Belum Diambil" : "Sudah Diambil";
                    }
                    echo $data['berkas']['statusBerkas'] . " - ";
                    stambil($data['berkas']['statusBerkas']);
                    ?>
                </td>
            </tr>
            <tr>
                <td>Tanggal Pengambilan</td>
                <td><?= $data['berkas']['diambilTanggal']; ?></td>
            </tr>
            <tr>
                <td>Diambil Oleh</td>
                <td><?= $data['berkas']['diambilOleh']; ?></td>
            </tr>
            <tr>
                <td>Surat Kuasa</td>
                <td><?= $data['berkas']['suratKuasa']; ?></td>
            </tr>

        </tbody>
    </table>
</body>

</html>