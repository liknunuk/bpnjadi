<?php
function tanggal($tgl)
{
    list($t, $b, $h) = explode("-", $tgl);
    return "$h-$b-$t";
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">DATA LENGKAP BERKAS SERTIPIKAT</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <table class="table table-sm">
                <tbody>
                    <tr>
                        <td>Nomor Berkas</td>
                        <td><?= $data['berkas']['nomorBerkas']; ?></td>
                    </tr>
                    <tr>
                        <td>Kegiatan</td>
                        <td><?= $data['berkas']['kegiatan']; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Pemilik</td>
                        <td><?= $data['berkas']['namaPemilik']; ?></td>
                    </tr>
                    <tr>
                        <td>Jenis Hak</td>
                        <td><?= $data['berkas']['jenisHak']; ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Hak</td>
                        <td><?= $data['berkas']['nomorHak']; ?></td>
                    </tr>
                    <tr>
                        <td>Desa , Kecamatan</td>
                        <td><?= $data['berkas']['namaDesa']; ?>, <?= $data['berkas']['kecamatan']; ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal Terima Loket</td>
                        <td><?= tanggal($data['berkas']['tanggaljadi']); ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Tanggungan</td>
                        <td><?= $data['berkas']['statusBerkas']; ?></td>
                    </tr>
                    <tr>
                        <td>Keterangan Lain</td>
                        <td><?= $data['berkas']['statusBerkas']; ?></td>
                    </tr>
                    <tr>
                        <td>Status Berkas</td>
                        <td><?php echo  $data['berkas']['statusBerkas'] == "0" ? "Sudah diambil" : "Belum Diambil"; ?></td>
                    </tr>
                    <tr>
                        <td>Diambil Tanggal</td>
                        <td><?= tanggal($data['berkas']['diambilTanggal']); ?></td>
                    </tr>
                    <tr>
                        <td>Diambil Oleh</td>
                        <td><?= $data['berkas']['diambilOleh']; ?></td>
                    </tr>
                    <!--tr>
                        <td></td>
                        <td><?= $data['berkas']['']; ?></td>
                    </tr -->
                    <tr>
                        <td colspan="2" class="text-center">
                            <a href="<?= BASEURL; ?>Notaris">Kembali</a>
                            <!-- <a href="<?= BASEURL; ?>Notaris/ceberk/<?= $data['berkas']['nomorBerkas']; ?>">Cetak</a> -->
                            <a href="javascript:void(0)" onClick=ceberk("<?= $data['berkas']['nomorBerkas']; ?>")>Cetak</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    function ceberk(nober) {
        window.open('<?= BASEURL; ?>Notaris/ceberk/' + nober, 'ceberk', 'width=600,height=1000,top=200,left=400px')
    }
</script>