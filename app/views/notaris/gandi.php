<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 mx-auto">
            <form action="<?= BASEURL; ?>Notaris/passcha" method="post">
                <div class="form-group row">
                    <label for="nikPengaju" class="col-sm-4">User ID</label>
                    <div class="col-sm-8">
                        <input type="text" name="nikPengaju" id="nikPengaju" class="form-control" readonly value="<?= $_SESSION['idppatk']; ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="katasandi" class="col-sm-4">Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" name="katasandi" id="katasandi" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="katasandi2" class="col-sm-4">Konfirmasi Password</label>
                    <div class="col-sm-8">
                        <input type="password" id="katasandi2" class="form-control">
                        <span id="passwordmatch"></span>
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <button type="submit" id="btn-submit" class="btn btn-success" disabled>Ganti Password</button>
                </div>
            </form>

        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $("#katasandi2").keyup(function() {
        let ks1 = $("#katasandi").val(),
            ks2 = $(this).val();
        if (ks1.length == ks2.length) {
            if (ks1 == ks2) {
                $("#btn-submit").prop("disabled", false);
                $("#passwordmatch").text('Password Sama');
            } else {
                $("#katasandi").val('');
                $("#katasandi2").val('');
                $("#passwordmatch").text('Password Tidak Sama');
            }
        }
    })
</script>