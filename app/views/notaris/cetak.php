<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $_SESSION['idppatk']; ?></title>
    <style>
        html,
        body {
            font-family: monospace;
            font-size: 10pt;
        }

        h4 {
            font-size: 24px;
        }

        @media print {
            .raketok {
                display: none;
            }
        }
    </style>
</head>

<body onload="window.print()">
    <!-- Tabel -->
    <center>
        <h4>DAFTAR SERTIPIKAT SUDAH JADI</h4>
    </center>
    <table>
        <tbody>
            <tr>
                <td width="200">User ID</td>
                <td><?= $_SESSION['idppatk']; ?></td>
            </tr>
            <tr>
                <td>Nama Notaris</td>
                <td><?= $_SESSION['namaLengkap']; ?></td>
            </tr>
            <tr>
                <td>Alamat &amp; Telepon&nbsp;&nbsp;</td>
                <td><?= $_SESSION['alamat']; ?> Telp: <?= $_SESSION['nomorTelepon']; ?></td>
            </tr>

        </tbody>
    </table>
    <table border="1" cellspacing="0" cellpadding="4" width="100%">
        <thead>
            <tr>
                <th>Nomor Berkas</th>
                <th>Kegiatan</th>
                <th>Nama Pemilik</th>
                <th>Desa</th>
                <th>Kecamatan</th>
                <th>Jenis Hak</th>
                <th>Nomor Hak</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['berkas'] as $berkas) : ?>
                <tr>
                    <td><?= $berkas['nomorBerkas']; ?></td>
                    <td><?= $berkas['kegiatan']; ?></td>
                    <td><?= $berkas['namaPemilik']; ?></td>
                    <td><?= $berkas['namadesa']; ?></td>
                    <td><?= $berkas['kec']; ?></td>
                    <td><?= $berkas['jenisHak']; ?></td>
                    <td><?= $berkas['nomorHak']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <a href="<?= BASEURL; ?>Notaris" class="raketok">Kembali</a>
    <!-- Tabel -->
</body>

</html>