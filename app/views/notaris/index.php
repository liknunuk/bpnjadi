<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <table class="mb-3">
                <tbody>
                    <tr>
                        <td colspan="2" class="pb-3">
                            <ul class="list-group list-group-horizontal">
                                <li class="list-group-item py-0"><a href="<?= BASEURL; ?>Notaris/cetak">Cetak</a></li>
                                <li class="list-group-item py-0"><a href="<?= BASEURL; ?>Notaris/gandi">Ganti Password</a></li>
                                <li class="list-group-item py-0"><a href="<?= BASEURL; ?>User/klilan/notaris">Logout</a></li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>User ID</td>
                        <td><?= $_SESSION['idppatk']; ?></td>
                    </tr>
                    <tr>
                        <td>Nama Notaris</td>
                        <td><?= $_SESSION['namaLengkap']; ?></td>
                    </tr>
                    <tr>
                        <td>Alamat &amp; Telepon&nbsp;&nbsp;</td>
                        <td><?= $_SESSION['alamat']; ?> Telp: <?= $_SESSION['nomorTelepon']; ?></td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 table-responsive">
            <?php Alert::sankil(); ?>
            <table class="table table-sm table-striped">
                <thead class="bg-primary">
                    <tr>
                        <th>Nomor Berkas</th>
                        <th>Kegiatan</th>
                        <th>Nama Pemilik</th>
                        <th>Desa</th>
                        <th>Kecamatan</th>
                        <th>Jenis Hak</th>
                        <th>Nomor Hak</th>
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['berkas'] as $berkas) : ?>
                        <tr>
                            <td><?= $berkas['nomorBerkas']; ?></td>
                            <td><?= $berkas['kegiatan']; ?></td>
                            <td><?= $berkas['namaPemilik']; ?></td>
                            <td><?= $berkas['namadesa']; ?></td>
                            <td><?= $berkas['kec']; ?></td>
                            <td><?= $berkas['jenisHak']; ?></td>
                            <td><?= $berkas['nomorHak']; ?></td>
                            <td><a href="<?= BASEURL; ?>Notaris/berkas/<?= $berkas['nomorBerkas']; ?>">Detil</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>