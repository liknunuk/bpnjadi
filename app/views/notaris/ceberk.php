<?php
function tanggal($tgl)
{
    list($t, $b, $h) = explode("-", $tgl);
    return "$h-$b-$t";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berkas <?= $data['berkas']['nomorBerkas']; ?></title>
    <style>
        html,
        body {
            font-family: monospace;
            font-size: 10pt;
        }

        h4 {
            font-size: 24px;
        }

        @media print {
            .raketok {
                display: none;
            }
        }
    </style>
</head>

<body onload="window.print()">
    <center>
        <h4>DATA BERKAS SERTIPIKAT No. <?= $data['berkas']['nomorBerkas']; ?></h4>
    </center>
    <table border="1" cellspacing="0" cellpadding="4" width="600" align="center">
        <tbody>
            <tr>
                <td>Nomor Berkas</td>
                <td><?= $data['berkas']['nomorBerkas']; ?></td>
            </tr>
            <tr>
                <td>Kegiatan</td>
                <td><?= $data['berkas']['kegiatan']; ?></td>
            </tr>
            <tr>
                <td>Nama Pemilik</td>
                <td><?= $data['berkas']['namaPemilik']; ?></td>
            </tr>
            <tr>
                <td>Jenis Hak</td>
                <td><?= $data['berkas']['jenisHak']; ?></td>
            </tr>
            <tr>
                <td>Nomor Hak</td>
                <td><?= $data['berkas']['nomorHak']; ?></td>
            </tr>
            <tr>
                <td>Desa , Kecamatan</td>
                <td><?= $data['berkas']['namaDesa']; ?>, <?= $data['berkas']['kecamatan']; ?></td>
            </tr>
            <tr>
                <td>Tanggal Terima Loket</td>
                <td><?= tanggal($data['berkas']['tanggaljadi']); ?></td>
            </tr>
            <tr>
                <td>Nomor Tanggungan</td>
                <td><?= $data['berkas']['statusBerkas']; ?></td>
            </tr>
            <tr>
                <td>Keterangan Lain</td>
                <td><?= $data['berkas']['statusBerkas']; ?></td>
            </tr>
            <tr>
                <td>Status Berkas</td>
                <td><?php echo  $data['berkas']['statusBerkas'] == "0" ? "Sudah diambil" : "Belum Diambil"; ?></td>
            </tr>
            <tr>
                <td>Diambil Tanggal</td>
                <td><?= tanggal($data['berkas']['diambilTanggal']); ?></td>
            </tr>
            <tr>
                <td>Diambil Oleh</td>
                <td><?= $data['berkas']['diambilOleh']; ?></td>
            </tr>
            <!--tr>
                        <td></td>
                        <td><?= $data['berkas']['']; ?></td>
                    </tr -->
            <tr>
                <td colspan="2" class="text-center">
                    <a href="<?= BASEURL; ?>Notaris" class="raketok">Kembali</a>
                </td>
            </tr>
        </tbody>
    </table>


    <?php $this->view('template/bs4js'); ?>


</body>

</html>