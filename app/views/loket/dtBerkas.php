<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">DATA LENGKAP BERKAS SERTIPIKAT</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <table class="table table-sm table-striped">
                <tbody>
                    <tr>
                        <th>Nomor Berkas</th>
                        <td><?= $data['berkas']['nomorBerkas']; ?></td>
                    </tr>
                    <tr>
                        <th>Kegiatan</th>
                        <td><?= $data['berkas']['kegiatan']; ?></td>
                    </tr>
                    <tr>
                        <th>Nama Pemilik</th>
                        <td><?= $data['berkas']['namaPemilik']; ?></td>
                    </tr>
                    <tr>
                        <th>Jenis Hak</th>
                        <td><?= $data['berkas']['jenisHak']; ?></td>
                    </tr>
                    <tr>
                        <th>Nomor Hak</th>
                        <td><?= $data['berkas']['nomorHak']; ?></td>
                    </tr>
                    <tr>
                        <th>Nomor Tanggungan</th>
                        <td><?= $data['berkas']['nomorTanggungan']; ?></td>
                    </tr>
                    <tr>
                        <th>Desa</th>
                        <td><?= $data['berkas']['namaDesa']; ?></td>
                    </tr>
                    <tr>
                        <th>Kecamatan</th>
                        <td><?= $data['berkas']['kecamatan']; ?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Penyerahan Loket</th>
                        <td><?= $data['berkas']['tanggaljadi']; ?></td>
                    </tr>
                    <tr>
                        <th>Keterangan Lain</th>
                        <td><?= $data['berkas']['keterangan']; ?></td>
                    </tr>
                    <tr>
                        <th>Status berkas</th>
                        <td><?php echo $data['berkas']['statusBerkas'] == '0' ? 'Sudah Diambil' : 'Belum Diambil'; ?></td>
                    </tr>
                    <tr>
                        <th>Pengambil</th>
                        <td>
                            <table class="table table-sm">
                                <tr>
                                    <td>No. KTP</td>
                                    <td id="takerKTP"><?= $data['berkas']['diambilOleh']; ?></td>
                                </tr>
                                <tr>
                                    <td>Nama Lengkap</td>
                                    <td id="takerNamaLengkap"><?= $data['taker']['namaLengkap']; ?></td>
                                </tr>
                                <tr>
                                    <td>Kategori</td>
                                    <td id="takerKategori"><?= $data['taker']['golongan']; ?></td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td id="takerAlamat"><?= $data['taker']['alamat']; ?></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Pengambilan</th>
                        <td><?= $data['berkas']['diambilTanggal']; ?></td>
                    </tr>
                </tbody>
            </table>
            <!-- 
                Array ( [nomorBerkas] => 0124-2021 [namaPemilik] => Oneng Erake Tulungan [kegiatan] => Pembuatan Sertifikat [jenisHak] => Hak Pakai [nomorHak] => HP/0124/2021 [namaDesa] => Berta [kecamatan] => Susukan [tanggaljadi] => 2021-05-02 [statusBerkas] => 0 [diambilTanggal] => 2021-05-02 [diambilOleh] => 3304065501820001 [suratKuasa] => https://google.com )
             -->
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>