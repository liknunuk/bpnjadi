<div class="container-fluid">
    <div class="row">
        <div class="col-md-12"><h4 class="page-title mt-5 text-center">FORMULIR PENGAMBILAN SERTIPIKAT</h4></div>
    </div>
    <div class="row mt-3">
        <div class="col-md-6 mx-auto">
            <form action="<?=BASEURL;?>Loket/ambilNambah" method="post" class="form-horizontal">
            
            <!--  nomorBerkas , diambilTanggal, diambilOleh , suratKuasa -->
            
                <div class="form-group row">
                    <label for="nomorBerkas" class="col-sm-4"> Nomor Berkas</label>
                    <div class="col-sm-8">
                        <input type="text" name="nomorBerkas" id="nomorBerkas" class="form-control" required value="<?=$_SESSION['stpkNomor'];?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="diambilTanggal" class="col-sm-4">Tanggal Pengambilan</label>
                    <div class="col-sm-8">
                        <input type="date" name="diambilTanggal" id="diambilTanggal" class="form-control" value="<?=date('Y-m-d');?>" >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="diambilOleh" class="col-sm-4">Diambil Oleh</label>
                    <div class="col-sm-8">
                        <input type="text" name="diambilOleh" id="diambilOleh" class="form-control" readonly required value="<?=$_SESSION['stpkNIK'];?>">
                        <table class="table table-sm table-bpn">
                            <tbody>
                                <tr>
                                    <td width="125">Nama Lengkap</td>
                                    <td><?=$data['pemohon']['namaLengkap'];?></td>
                                </tr>
                                <tr>
                                    <td>Golongan</td>
                                    <td><?=$data['pemohon']['golongan'];?></td>
                                </tr>
                                <tr>
                                    <td>Nomor Telepon</td>
                                    <td><?=$data['pemohon']['nomorTelepon'];?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="suratKuasa" class="col-sm-4">Surat Kuasa</label>
                    <div class="col-sm-8">
                        <input type="text" name="suratKuasa" id="suratKuasa" class="form-control" placeholder="link surat kuasa, jika ada">
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center">
                    <button class="btn btn-primary">Proses !</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>