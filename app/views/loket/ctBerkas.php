<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berkas<?= $data['berkas']['nomorBerkas']; ?></title>
    <style>
        th {
            text-align: left;
            padding: 5px 15px;
        }
    </style>
</head>

<body onLoad="window.print()">
    <center>
        <h2>Data Berkas Sertipikat Nomor: <?= $data['berkas']['nomorBerkas']; ?></h2>
    </center>
    <table border="1" cellspacing="0" class="main" width="800" align="center">
        <tbody>
            <tr>
                <th width="250">Nomor Berkas</th>
                <td><?= $data['berkas']['nomorBerkas']; ?></td>
            </tr>
            <tr>
                <th>Kegiatan</th>
                <td><?= $data['berkas']['kegiatan']; ?></td>
            </tr>
            <tr>
                <th>Nama Pemilik</th>
                <td><?= $data['berkas']['namaPemilik']; ?></td>
            </tr>
            <tr>
                <th>Jenis Hak</th>
                <td><?= $data['berkas']['jenisHak']; ?></td>
            </tr>
            <tr>
                <th>Nomor Hak</th>
                <td><?= $data['berkas']['nomorHak']; ?></td>
            </tr>
            <tr>
                <th>Nomor Tanggungan</th>
                <td><?= $data['berkas']['nomorTanggungan']; ?></td>
            </tr>
            <tr>
                <th>Desa</th>
                <td><?= $data['berkas']['namaDesa']; ?></td>
            </tr>
            <tr>
                <th>Kecamatan</th>
                <td><?= $data['berkas']['kecamatan']; ?></td>
            </tr>
            <tr>
                <th>Tanggal Penyerahan Loket</th>
                <td><?= $data['berkas']['tanggaljadi']; ?></td>
            </tr>
            <tr>
                <th>Keterangan Lain</th>
                <td><?= $data['berkas']['keterangan']; ?></td>
            </tr>
            <tr>
                <th>Status berkas</th>
                <td><?php echo $data['berkas']['statusBerkas'] == '0' ? 'Sudah Diambil' : 'Belum Diambil'; ?></td>
            </tr>
            <tr>
                <th>Pengambil</th>
                <td>
                    <table border="0" cellspacing="0" class="sub">
                        <tr>
                            <td width="200">No. KTP</td>
                            <td id="takerKTP"><?= $data['berkas']['diambilOleh']; ?></td>
                        </tr>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td id="takerNamaLengkap"><?= $data['taker']['namaLengkap']; ?></td>
                        </tr>
                        <tr>
                            <td>Kategori</td>
                            <td id="takerKategori"><?= $data['taker']['golongan']; ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td id="takerAlamat"><?= $data['taker']['alamat']; ?></td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <th>Tanggal Pengambilan</th>
                <td><?= $data['berkas']['diambilTanggal']; ?></td>
            </tr>
        </tbody>
    </table>

    <?php $this->view('template/bs4js'); ?>
</body>

</html>