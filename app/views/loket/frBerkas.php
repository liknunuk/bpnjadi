<?php
function keg($kt, $kf)
{
    // $kt = Keg. tabel. $kf = Keg. Formulir
    return $kf == $kt ? "Selected" : NULL;
}

function hak($ht, $hf)
{
    return $ht == $hf ? 'Selected' : NULL;
}

function setRO($modus)
{
    if ($modus == 'gantos') echo "readonly";
}
$berkas = $data['berkas'];
// print_r($berkas);
// exit();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">FORMULIR BERKAS SERTIPIKAT JADI</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 mx-auto">
            <form action="<?= BASEURL; ?>Loket/berkasTambah" method="post" class="form-horizontal">
                <input type="hidden" name="modus" value="<?= $data['modus']; ?>">
                <div class="form-group row">
                    <label for="diajukanOleh" class="col-sm-4">NIK yang mengajukan</label>
                    <div class="col-sm-8">
                        <?php
                        // print_r($_SESSION);
                        if (isset($_SESSION['stpkNIK'])) {
                            $nikPemohon = $_SESSION['stpkNIK'];
                            // echo "session is set";
                        } else {
                            $nikPemohon = $berkas['diajukanOleh'];
                            // echo "session is unset";
                        }
                        ?>
                        <input type="text" name="diajukanOleh" id="diajukanOleh" class="form-control" placeHolder="No KTP yang mengajukan" required value="<?= $nikPemohon; ?>">
                        <span id="npconfirm"></span>
                        <?php unset($_SESSION['stpkNIK']);
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kegiatan" class="col-sm-4">Kegiatan</label>
                    <div class="col-sm-8">
                        <select name="kegiatan" id="kegiatan" class="form-control">
                            <?php
                            $jenisKegiatan = ['Pembuatan Sertipikat', 'Roya', 'Pengecekan', 'Ganti Nama', 'Pemecahan Sertipikat', 'Peralihan Hak - Jual Beli', 'Peralihan Hak - Waris', 'Peralihan Hak - Hibah', 'Peralihan Hak - Hak Bersama', 'Pencatatan Perubahan Penggunaan Tanah', 'Pemisahan', 'Ijin Lokasi', 'Ijin Perubahan Penggunaan Tanah', 'Ijin Peralihan Hak', 'Tapak Kavling'];
                            sort($jenisKegiatan);
                            ?>
                            <?php foreach ($jenisKegiatan as $i => $kegiatan) : ?>
                                <option <?php echo keg($berkas['kegiatan'], $kegiatan); ?> value="<?= $kegiatan; ?>"><?= $kegiatan; ?></option>
                            <?php endforeach; ?>
                            <!--                             
                            <option <?= keg($berkas['kegiatan'], 'Roya'); ?> value="Roya">Roya</option>
                            <option <?= keg($berkas['kegiatan'], 'Pengecekan'); ?> value="Pengecekan">Pengecekan Sertipikat</option>
                            <option <?= keg($berkas['kegiatan'], 'Ganti Nama'); ?> value="Ganti Nama">Ganti Nama</option>
                            <option <?= keg($berkas['kegiatan'], 'Pemecahan Sertipikat'); ?> value="Pemecahan Sertipikat">Pemecahan Sertipikat</option>
                            <option <?= keg($berkas['kegiatan'], 'Peralihan Hak - Jual Beli'); ?> value="Peralihan Hak - Jual Beli">Peralihan Hak - Jual Beli</option>
                            <option <?= keg($berkas['kegiatan'], 'Peralihan Hak - Waris'); ?> value="Peralihan Hak - Waris">Peralihan Hak - Waris</option>
                            <option <?= keg($berkas['kegiatan'], 'Peralihan Hak - Hibah'); ?> value="Peralihan Hak - Hibah">Peralihan Hak - Hibah</option>
                            <option <?= keg($berkas['kegiatan'], 'Peralihan Hak - Hak Bersama'); ?> value="Peralihan Hak - Hak Bersama">Peralihan Hak - Hak Bersama</option>
                            <option <?= keg($berkas['kegiatan'], 'Pencatatan Perubahan Penggunaan Tanah'); ?> value="Pencatatan Perubahan Penggunaan Tanah">Pencatatan Perubahan Penggunaan Tanah</option>
                            <option <?= keg($berkas['kegiatan'], 'Pemisahan'); ?> value="Pemisahan">Pemisahan</option>
                            <option <?= keg($berkas['kegiatan'], 'Ijin Lokasi'); ?> value="Ijin Lokasi">Ijin Lokasi</option>
                            <option <?= keg($berkas['kegiatan'], 'Ijin Perubahan Penggunaan Tanah'); ?> value="Ijin Perubahan Penggunaan Tanah">Ijin Perubahan Penggunaan Tanah</option>
                            <option <?= keg($berkas['kegiatan'], 'Ijin Peralihhan Hak'); ?> value="Ijin Peralihhan Hak">Ijin Peralihhan Hak</option>
                            <option <?= keg($berkas['kegiatan'], 'Tapak Kavling'); ?> value="Tapak Kavling">Tapak Kavling</option> -->
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nomorBerkas" class="col-sm-4">Nomor Berkas</label>
                    <div class="col-sm-8">
                        <input type="text" name="nomorBerkas" id="nomorBerkas" class="form-control" required value="<?php list($n, $t) = explode('-', $berkas['nomorBerkas']);
                                                                                                                    echo $n; ?>" <?php setRO($data['modus']); ?>>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tahunBerkas" class="col-sm-4">Tahun Berkas</label>
                    <div class="col-sm-8">
                        <input type="number" name="tahunBerkas" id="tahunBerkas" class="form-control" value="<?= $berkas['tahunBerkas']; ?>" <?php setRO($data['modus']); ?>>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="namaPemilik" class="col-sm-4">Nama Lengkap Pemilik</label>
                    <div class="col-sm-8">
                        <input type="text" name="namaPemilik" id="namaPemilik" class="form-control" required value="<?= $berkas['namaPemilik']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nikPemilik" class="col-sm-4">NIK / NO. KTP Pemilik</label>
                    <div class="col-sm-8">
                        <input type="text" name="nikPemilik" id="nikPemilik" class="form-control" value="<?= $berkas['nikPemilik']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="jenisHak" class="col-sm-4">Jenis Hak</label>
                    <div class="col-sm-8">
                        <select name="jenisHak" id="jenisHak" class="form-control">
                            <option <?php echo hak($berkas['jenisHak'], 'Hak Milik'); ?> value="Hak Milik" selected>Hak Milik</option>
                            <option <?php echo hak($berkas['jenisHak'], 'Hak Guna Usaha'); ?> value="Hak Guna Usaha">Hak Guna Usaha</option>
                            <option <?php echo hak($berkas['jenisHak'], 'Hak Guna Bangunan'); ?> value="Hak Guna Bangunan">Hak Guna Bangunan</option>
                            <option <?php echo hak($berkas['jenisHak'], 'Hak Pakai'); ?> value="Hak Pakai">Hak Pakai</option>
                            <option <?php echo hak($berkas['jenisHak'], 'Hak Waqaf'); ?> value="Hak Waqaf">Hak Waqaf</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nomorHak" class="col-sm-4">Nomor Hak</label>
                    <div class="col-sm-8">
                        <input type="text" name="nomorHak" id="nomorHak" class="form-control" required value="<?= $berkas['nomorHak']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="nomorTanggungan" class="col-sm-4">Nomor Tanggungan</label>
                    <div class="col-sm-8">
                        <input type="text" name="nomorTanggungan" id="nomorTanggungan" class="form-control" required value="<?= $berkas['nomorTanggungan']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tanggalJadi" class="col-sm-4">Tanggal Penyerahan Loket</label>
                    <div class="col-sm-8">
                        <input type="date" name="tanggalJadi" id="tanggalJadi" class="form-control" value="<?= date('Y-m-d'); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="desa" class="col-sm-4">Desa</label>
                    <div class="col-sm-8">
                        <input type="text" name="desa" id="desa" class="form-control" value="<?= $berkas['desa']; ?>">
                        <table class="table table-sm" style="display:none;" id="tbDesa">
                            <thead>
                                <tr>
                                    <th>Desa</th>
                                    <th>Kecamatan</th>
                                </tr>
                            </thead>
                            <tbody id="kodeDesa"></tbody>
                        </table>
                        <table class="table table-sm">
                            <tbody>
                                <tr>
                                    <td>Desa</td>
                                    <td id="ketDesa"><?php echo $data['wilayah'] != NULL ? $data['wilayah']['namaDesa'] : ""; ?></td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td id="ketCmtn"><?php echo $data['wilayah'] != NULL ? $data['wilayah']['kecamatan'] : ""; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="keterangan" class="col-sm-4">Keterangan</label>
                    <div class="col-sm-8">
                        <input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan Pemecahan Hak" value="<?= $berkas['keterangan']; ?>">
                    </div>
                </div>

                <div class="form-group d-flex justify-content-end pr-3">
                    <button class="btn btn-primary">Simpan</button>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- Modals -->
<!-- Modal Customer -->
<div class="modal" tabindex="-1" role="dialog" id="mdKustomer">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengguna Jasa BPN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>Loket/pemohonTambah" method="post" class="form-horizontal">
                    <input type="hidden" name="mode" id="mode" value="baru">
                    <div class="form-group row">
                        <label for="nikPengaju" class="col-sm-3">NIK</label>
                        <div class="col-sm-9">
                            <input type="text" name="nikPengaju" id="nikPengaju" class="form-control" required maxlength='16'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="golongan" class="col-sm-3">Golongan</label>
                        <div class="col-sm-9">
                            <select name="golongan" id="golongan" class="form-control">
                                <option value="Perorangan">Perorangan</option>
                                <option value="Notaris">Notaris</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="namaLengkap" class="col-sm-3">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" required>
                        </div>
                    </div>
                    <!-- nikPengaju , golongan , namaLengkap , alamat, nomorTelepon , email -->

                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3">Alamat</label>
                        <div class="col-sm-9">
                            <input type="text" name="alamat" id="alamat" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nomorTelepon" class="col-sm-3">Nomor Telepon</label>
                        <div class="col-sm-9">
                            <input type="text" name="nomorTelepon" id="nomorTelepon" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-3">Alamat E-mail</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="katasandi" class="col-sm-3">Kata Sandi</label>
                        <div class="col-sm-9">
                            <input type="text" name="katasandi" id="katasandi" class="form-control" value="ppatk2021">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Customer -->
<?php $this->view('template/bs4js'); ?>
<script>
    $('#desa').keyup(function() {
        let desa = this.value;
        if (desa.length >= 4) {
            $.ajax({
                method: 'get',
                datatype: 'json',
                url: "<?= BASEURL; ?>Loket/desa/" + desa,
                success: function(res) {
                    $('#kodeDesa tr').remove();
                    $('#tbDesa').show();
                    $.each(JSON.parse(res), function(i, data) {
                        $('#kodeDesa').append(`
                    <tr>
                    <td class='infoDesa' id='${data.kode}'><a href='javascript:void(0)'>${data.desa}</a></td>
                    <td>${data.kecamatan}</td>
                    </tr>
                    `)
                    })
                }
            })
        }
    })

    $('#kodeDesa').on('click', '.infoDesa', function() {
        // let kode = this.id;
        $('#desa').val(this.id);
        $('#ketDesa').text($(this).children('a').text());
        $('#ketCmtn').text($(this).parent().children('td:nth-child(2)').text());
        $('#kodeDesa tr').remove();
        $('#tbDesa').hide();
    })

    $('#diajukanOleh').blur(function() {
        let np = this.value;
        $.post('<?= BASEURL; ?>Loket/cekPemohon', {
            nikPemohon: np
        }, function(res) {
            // console.log(res);
            if (res == "0") {
                $('#mdKustomer').modal('show');
                $('#nikPengaju').val(np);
            }
            $('#npconfirm').text(res)
        })
    })
</script>