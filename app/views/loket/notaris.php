<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">DAFTAR NOTARIS</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#mdKustomer">Tambah Notaris</a>
            <!-- nikPengaju , namaLengkap , alamat , nomorTelepon , email -->
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <thead>
                        <tr class="text-center">
                            <th>ID Notaris</th>
                            <th>Nama Lengkap</th>
                            <th>Alamat</th>
                            <th>Nomor Telepon</th>
                            <th>Email</th>
                            <th width="125"><i class="fa fa-gear"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data['ppatk'] as $notaris) : ?>
                            <tr>
                                <td><?= $notaris['nikPengaju']; ?></td>
                                <td><?= $notaris['namaLengkap']; ?></td>
                                <td><?= $notaris['alamat']; ?></td>
                                <td><?= $notaris['nomorTelepon']; ?></td>
                                <td><?= $notaris['email']; ?></td>
                                <td>
                                    <a href="javascript:void(0)" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modals -->
<!-- Modal Customer -->
<div class="modal" tabindex="-1" role="dialog" id="mdKustomer">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengguna Jasa BPN</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>Loket/notarisTambah" method="post" class="form-horizontal">
                    <input type="hidden" name="mode" id="mode" value="baru">
                    <div class="form-group row">
                        <label for="nikPengaju" class="col-sm-3">NIK</label>
                        <div class="col-sm-9">
                            <input type="text" name="nikPengaju" id="nikPengaju" class="form-control" required maxlength='16'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="namaLengkap" class="col-sm-3">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" required>
                        </div>
                    </div>
                    <!-- nikPengaju , golongan , namaLengkap , alamat, nomorTelepon , email -->

                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3">Alamat</label>
                        <div class="col-sm-9">
                            <input type="text" name="alamat" id="alamat" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nomorTelepon" class="col-sm-3">Nomor Telepon</label>
                        <div class="col-sm-9">
                            <input type="text" name="nomorTelepon" id="nomorTelepon" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-3">Alamat E-mail</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="katasandi" class="col-sm-3">Kata Sandi</label>
                        <div class="col-sm-9">
                            <input type="text" name="katasandi" id="katasandi" class="form-control" value="ppatk2021">
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Customer -->
<?php $this->view('template/bs4js'); ?>
<script>
    $('.btn-success').click(function() {
        let nikPengaju = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        $.getJSON('<?= BASEURL; ?>Loket/dataNotaris/' + nikPengaju, function(res) {
            console.log(res);
            $('#mdKustomer').modal('show');
            $('#nikPengaju').val(res.nikPengaju);
            $('#nikPengaju').prop('readonly', true);
            $('#katasandi').hide();
            $('#namaLengkap').val(res.namaLengkap);
            $('#alamat').val(res.alamat);
            $('#nomorTelepon').val(res.nomorTelepon);
            $('#email').val(res.email);
            $('#mode').val('ubah');
        });
    });

    $('.btn-danger').click(function() {
        let nikPengaju = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        alert('Notaris ' + nikPengaju + ' Akan Dihapus');
        $.post("<?= BASEURL; ?>Loket/pengajuSampah", {
            nikPengaju: nikPengaju
        }, function(res) {
            if (res == "1") {
                alert('Notaris telah dihapus');
                location.reload();
            }
        })
    })
</script>