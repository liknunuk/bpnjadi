<div class="container-fluid">
  <div class="row loket-appBar py-3">
    <div class="col-md-2" id="appLogo">
      <img src="https://upload.wikimedia.org/wikipedia/commons/5/51/Logo_BPN-KemenATR_%282017%29.png" alt="logo-bpn">
    </div>
    <div class="col-md-10">
      <h1>INFORMASI PENERBITAN SERTIPIKAT</h1>
      <h3>BADAN PERTANAHAN KABUPATEN BANJARNEGARA</h3>
    </div>
  </div>

  <!-- <div class="row">
        <div class="col-lg-12">
        </div>
    </div> -->
</div>
<!-- navigasi -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?= BASEURL; ?>Loket">BPN</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarLoket" aria-controls="navbarLoket" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarLoket">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL; ?>Loket">Berkas Jadi</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL; ?>Loket/kustomer/ab">Pengambilan Berkas</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL; ?>Loket/notaris">Notaris</a>
      </li>

    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a href="#" class="nav-link text-light"><?= $_SESSION['fullName']; ?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL . "User/klilan/loket"; ?>">Logout</a>
      </li>
    </ul>
  </div>
</nav>
<!-- navigasi -->