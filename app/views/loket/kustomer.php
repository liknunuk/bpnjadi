<?php
if($data['flow'] == 'bb'){
    $formTitle = "Pemohon";
}else{
    $formTitle = "Pengambil";
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">PENCARIAN DATA <?=strtoupper($formTitle);?> SERTIPIKAT</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 mx-auto">
            <p class='text-center'>Masukkan Identitas <?=$formTitle;?></p>
            <input type="text" id="nikCari" class="form-control bg-redup mb-2 text-center" placeholder="NIK <?=$formTitle;?>" value="3304065501820001" >
            <div id="hasilCari"></div>

            <table class="table table-sm table-bpn">
            <!-- nikPengaju , golongan , namaLengkap , alamat, nomorTelepon , email -->
                <tbody>
                    <tr>
                        <td colspan="2" id="hasilCari" class="text-center">
                        Berkas Nomor <?=$data['berkas']; ?>
                        <span class="float-right"><a href="javascript:void(o)" data-toggle="modal" data-target="#mdKustomer"><?=$formTitle;?> Baru</a></span>
                        </td>
                    </tr>
                    <tr>
                        <th width="125">Nama Lengkap</th>
                        <td id="namaLengkap"></td>
                    </tr>
                    <tr>
                        <th>Kelompok</th>
                        <td id="golongan"></td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td id="alamat"></td>
                    </tr>
                    <tr>
                        <th>Nomor Telepon</th>
                        <td id="nomorTelepon"></td>
                    </tr>
                    <tr>
                        <th>Surat Elektronik</th>
                        <td id="email"></td>
                    </tr>
                </tbody>
            </table>
            <?php 
            if( $data['flow'] == "bb" ) {
                $label = "Tambah Berkas";
                $terus = BASEURL."Loket/frBerkas";
            }else{
                $label = "Ambil Berkas";
                $terus = BASEURL."Loket/amBerkas";
            }
            ?>
            <div class="d-flex justify-content-center">
                <!-- <button class="btn btn-primary px-5 disabled"><?=$label;?></button> -->
                <button class="btn btn-primary" style="display:none;" id="frAmbil"><?=$label;?></button>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<!-- Modal Customer -->
<div class="modal" tabindex="-1" role="dialog" id="mdKustomer">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pengguna Jasa BPN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL;?>Loket/pengajuTambah" method="post" class="form-horizontal">
            
            <div class="form-group row">
                <label for="nikPengaju" class="col-sm-3">NIK</label>
                <div class="col-sm-9">
                    <input type="text" name="nikPengaju" id="nikPengaju" class="form-control" required maxlength='16'>
                </div>
            </div>

            <div class="form-group row">
                <label for="golongan" class="col-sm-3">Kelompok</label>
                <div class="col-sm-9">
                    <select name="golongan" id="golongan" class="form-control">
                        <option value="Perorangan">Perorangan</option>
                        <option value="Notaris">Notaris</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="namaLengkap" class="col-sm-3">Nama Lengkap</label>
                <div class="col-sm-9">
                    <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" required>
                </div>
            </div>
            <!-- nikPengaju , golongan , namaLengkap , alamat, nomorTelepon , email -->

            <div class="form-group row">
                <label for="alamat" class="col-sm-3">Alamat</label>
                <div class="col-sm-9">
                    <input type="text" name="alamat" id="alamat" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomorTelepon" class="col-sm-3">Nomor Telepon</label>
                <div class="col-sm-9">
                    <input type="text" name="nomorTelepon" id="nomorTelepon" class="form-control">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-3">Alamat E-mail</label>
                <div class="col-sm-9">
                    <input type="email" name="email" id="email" class="form-control">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
    </form>
    </div>
  </div>
</div>
<!-- Modal Customer -->

<?php $this->view('template/bs4js'); ?>
<script>
$('#frAmbil').click( function(){
    location.href="<?=BASEURL;?>Loket/frAmbil";
})

$('#nikCari').keypress( function(e){
    if(e.keyCode == 13){
        e.preventDefault();
        $.ajax({
            dataType    : 'json',
            url         : '<?=BASEURL;?>Loket/pengajuNgarah',
            method      : 'POST',
            data        : { nik:$(this).val() },
            success     : function(res){
                let hasil = res.hasil;
                let data = res.data;
                $("#hasilCari").html(hasil);
                if(hasil == 'Ditemukan'){
                    $('#namaLengkap').text(data.namaLengkap);
                    $('#golongan').text(data.golongan);
                    $('#alamat').text(data.alamat);
                    $('#nomorTelepon').text(data.nomorTelepon);
                    $('#email').text(data.email);
                    $('#frAmbil').show();
                }else{
                    $('#namaLengkap').text('');
                    $('#golongan').text('');
                    $('#alamat').text('');
                    $('#nomorTelepon').text('');
                    $('#email').text('');
                    $('#frAmbil').hide();
                }
            }
        })
    }
})

</script>
<!-- 
    <td id="namaLengkap"></td>
    <td id="golongan"></td>
    <td id="alamat"></td>
    <td id="nomorTelepon"></td>
    <td id="email"></td> 
-->