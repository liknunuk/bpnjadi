<?php
function tanggalan($tanggal)
{
    list($y, $m, $d) = explode('-', $tanggal);
    return ("$d - $m - $y");
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row mt-3">
                <div class="col-md-2 py-2">
                    <a class="align-middle" href="<?= BASEURL; ?>Loket/frBerkas"><i class="fas fa-plus"> Berkas</i></a>
                </div>
                <div class="col-md-7">
                    <h4 class="mt-3 text-center page-title">DAFTAR SERTIPIKAT BELUM DIAMBIL</h4>
                </div>
                <div class="col-md-3">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Masukkan No. Berkas" aria-label="Masukkan No. Berkas" aria-describedby="datakunci" id="nomorPencarian">
                        <div class="input-group-append">
                            <span class="input-group-text" id="datakunci"><i class="fas fa-search text-dark"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <?php Alert::sankil(); ?>
            <table class="table table-sm table-bpn">
                <thead>
                    <tr>
                        <th rowspan="2">Kegiatan</th>
                        <th rowspan="2">Nomor Berkas</th>
                        <th rowspan="2">Nama Pemilik</th>
                        <th colspan="2">Hak</th>
                        <th rowspan="2">Tanggal Penyerahan Loket</th>
                        <th rowspan="2">
                            <i class="fas fa-gear"></i>
                        </th>
                    </tr>
                    <tr>
                        <!-- <th>Nomor</th>
                        <th>Tahun</th> -->
                        <!-- <th>Nama Lengkap</th> -->
                        <!-- <th>NIK / No. KTP</th> -->
                        <th>Jenis</th>
                        <th>Nomor</th>
                    </tr>
                </thead>
                <tbody id="blmAmbil">
                    <?php foreach ($data['blmAmbil'] as $jadi) : ?>
                        <tr>
                            <td><?= $jadi['kegiatan']; ?></td>
                            <td><?= $jadi['nomorBerkas']; ?></td>
                            <!-- <td><?= $jadi['tahunBerkas']; ?></td> -->
                            <td><?= $jadi['namaPemilik']; ?></td>
                            <!-- <td><?= $jadi['nikPemilik']; ?></td> -->
                            <td><?= $jadi['jenisHak']; ?></td>
                            <td><?= $jadi['nomorHak']; ?></td>
                            <td class='text-right'><?= tanggalan($jadi['tanggalJadi']); ?></td>
                            <td class='text-center'>
                                <a href="<?= BASEURL; ?>Loket/frBerkas/<?= $jadi['nomorBerkas']; ?>"><i class="fas fa-edit ikon"></i></a>
                                <a href="javascript:void(0)"><i class="fas fa-eraser ikon"></i></a>
                                <a href="<?= BASEURL; ?>Loket/kustomer/ab/<?= $jadi['nomorBerkas']; ?>"><i class="fas fa-handshake ikon"></i></a>
                                <a href="javascript:void(0)" class="cetak" id="ct_<?= $jadi['nomorBerkas']; ?>"><i class="fas fa-print ikon"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <!-- Berkas Sudah Diambil -->
            <div class="row mt-5">
                <div class="col-md-2 py-2">
                    &nbsp;
                </div>
                <div class="col-md-7">
                    <h4 class="mt-3 text-center page-title">DAFTAR SERTIPIKAT SUDAH DIAMBIL</h4>
                </div>
                <div class="col-md-3">
                    <div class="input-group mb-3" style="visibility:hidden;">
                        <input type="text" class="form-control" placeholder="Masukkan No. Berkas / NIK Pemilik" aria-label="Masukkan No. Berkas / NIK Pemilik" aria-describedby="datakunci">
                        <div class="input-group-append">
                            <span class="input-group-text" id="datakunci"><i class="fas fa-search text-dark"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-sm table-bpn">

                <thead>
                    <tr>
                        <th rowspan="2">Kegiatan</th>
                        <th rowspan="2">Nomor Berkas</th>
                        <th rowspan="2">Nama Pemilik</th>
                        <th colspan="2">Hak</th>
                        <th rowspan="2">Data Pengambilan</th>
                        <th rowspan="2">
                            <i class="fas fa-gear"></i>
                        </th>
                    </tr>
                    <tr>
                        <!-- <th>Nomor</th>
                        <th>Tahun</th> -->
                        <!-- <th>Nama Lengkap</th> -->
                        <!-- <th>NIK / No. KTP</th> -->
                        <th>Jenis</th>
                        <th>Nomor</th>
                    </tr>
                </thead>
                <tbody id="sdhAmbil">
                    <?php foreach ($data['sdhAmbil'] as $ambil) : ?>
                        <tr>
                            <td><?= $ambil['kegiatan']; ?></td>
                            <td><?= $ambil['nomorBerkas']; ?></td>
                            <td><?= $ambil['namaPemilik']; ?></td>
                            <td><?= $ambil['jenisHak']; ?></td>
                            <td><?= $ambil['nomorHak']; ?></td>
                            <td>Oleh: <?= $ambil['namaLengkap']; ?> ( <?= $ambil['nikPengaju']; ?> ) tanggal: <?= tanggalan($ambil['diambilTanggal']); ?></td>
                            <td>
                                <a href="javascript:void(0)" class="berkasDetail" id="<?= $ambil['nomorBerkas']; ?>"><i class="fas fa-info-circle"></i></a> detail

                                <a href="javascript:void(0)" class="cetak" id="ct_<?= $ambil['nomorBerkas']; ?>"><i class="fas fa-print ikon"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $(".berkasDetail").click(function() {
        let nb = this.id;
        window.location.href = '<?= BASEURL; ?>Loket/berkasDetail/' + nb;
    })

    $('#nomorPencarian').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            let nb = this.value;
            let ib = nb.replace(/\//g, "-");
            window.location.href = '<?= BASEURL; ?>Loket/berkasDetail/' + ib;
        }
    })

    $('.fa-eraser').click(function() {
        let nomorBerkas = $(this).parent('a').parent('td').parent('tr').children('td:nth-child(2)').text();
        let tenan = confirm('Berkas ' + nomorBerkas + ' Akan Dihapus?');
        if (tenan == true) {
            $.post("<?= BASEURL; ?>Loket/berkasSampah", {
                nomorBerkas: nomorBerkas
            }, function(res) {
                if (res == "1") {
                    alert('Berkas Berhasil Dihapus');
                    location.reload();
                }
            })
        }
    })

    $(".cetak").click(function() {
        let idb = this.id;
        let nmb = idb.split('_');
        let nomorBerkas = nmb[1];
        window.open("<?= BASEURL; ?>Loket/berkasCetak/" + nomorBerkas, "berkas", "width=1000,height=1200,top=200,left=300");
    })
</script>