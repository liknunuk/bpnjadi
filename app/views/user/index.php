<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 mx-auto">

            <div class="d-flex justify-content-center">
                <h3 class="my-3">Login <?= $data['gol']; ?></h3>
            </div>

            <form action="<?= BASEURL; ?>User/userAuth" method="post" class='mt-3'>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="Username"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="text" name="userName" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="username">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="password"><i class="fas fa-lock"></i></span>
                    </div>
                    <input type="password" name="userPass" class="form-control" placeholder="password" aria-label="password" aria-describedby="password" value="agraria234">
                </div>

                <div class="form-group">
                    <button class="btn btn-success form-control">Login</button>
                </div>

            </form>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>