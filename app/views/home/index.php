<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 mx-auto text-center">
            <h4 class="mt-5 mb-3">Informasi pencarian sertipikat jadi.</h4>
            <p class="lead">Silakan masukkan nomor berkas pencarian pada kolom berikut dengan format <strong>xxxx-yyyy</strong>. x = 4 digit nomor urut. y = 4 digit tahun pengajuan.</p>
            <!-- Form Pencarian Berkas -->
            <table class="table mt-3 mx-auto" style="width:100%;">
                <tbody>
                    <tr>
                        <td>
                            <div class="form-group">
                                <input type="text" class="form-control text-center" id="nomorBerkas" placeholder="0123-2021"><br>
                            </div>
                        </td>
                        <td width="125px">
                            <button id="cariBerkas" class="btn btn-primary">Cari Berkas!</button>
                        </td>
                    </tr>
                    <tr>
                        <td id="hasilPencarian"></td>
                    </tr>
                </tbody>
            </table>
            <div class="text-center"><a href="<?= BASEURL; ?>User/notaris">Login Notaris</a></div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('#cariBerkas').click(function() {
        let teks, staber;
        let nb = $('#nomorBerkas').val();
        $.ajax({
            dataType: 'json',
            url: `<?= BASEURL; ?>Home/detil/${nb}`,
            success: function(res) {
		console.log(res);
		console.log('cacah: ',res.cacah);
		console.log('status: ',res.staber);
                $('#hasilPencarian').text('');
                if (res.cacah == "0") {
                    teks = 'Berkas belum jadi. ';
                    staber = "";
                } else {
                    teks = 'Berkas sudah jadi. ';
                    if (res.staber == '1') {
                        staber = 'Berkas Belum Diambil';
                    } else {
                        staber = 'Berkas Sudah Diambil';
                    }
                }
                $('#hasilPencarian').text(teks + staber);
            }
        })
    })
</script>
