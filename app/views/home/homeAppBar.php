<div class="jumbotron jumbotron-fluid client-appBar">
  <div class="container">
      <div class="row">
          <div class="col-md-2 text-center" id="appLogo">
              <img src="https://upload.wikimedia.org/wikipedia/commons/5/51/Logo_BPN-KemenATR_%282017%29.png" alt="logo-bpn">
          </div>
          <div class="col-md-10 text-center">
              <h1 class="display-4">INFORMASI PENERBITAN SERTIPIKAT</h1>
              <h3>BADAN PERTANAHAN NASIONAL KABUPATEN BANJARNEGARA</h3>
          </div>
      </div>
  </div>
</div>