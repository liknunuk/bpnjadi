<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 mb-5">
            <h4 class="page-title mt-5 text-center">LOGIN ADMIN</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mx-auto" style="min-height:40vh; background-color:lightgray; margin-top:10px;">
            <form action="<?= BASEURL; ?>Admin/auth" method="post" style="margin-top:100px">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="userName"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="text" name="userName" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="username">
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="userPass"><i class="fas fa-lock"></i></span>
                    </div>
                    <input type="password" name="userPass" class="form-control" placeholder="Password" aria-label="password" aria-describedby="password" value="agraria234">
                </div>

                <div class="form-group">
                    <button class="btn btn-success form-control">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>