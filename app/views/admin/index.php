<?php
function genpass()
{
    $if1 = mt_rand(0, 9);
    $fr1 = ['Galunggung', 'Slamet', 'Sindoro', 'Sumbing', 'Merapi', 'Merbabu', 'Lawu', 'Kelud', 'Bromo', 'Ciremai'];
    $fr2 = mt_rand(101, 299);
    $password = $fr1[$if1] . $fr2;
    return $password;
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="page-title mt-5 text-center">PENGGUNA SISTEM</h4>
        </div>
    </div>
    <?php Alert::sankil(); ?>
    <div class="row">
        <div class="col">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#mdUser"><i class="fas fa-plus"> Pengguna</i></a>
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Nama Lengkap</th>
                        <th>Nama Pengguna</th>
                        <th>Hak Akses</th>
                        <th class="text-center" width="175px"><i class="fas fa-gear"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['users'] as $user) : ?>
                        <tr>
                            <td><?= $user['userID']; ?></td>
                            <td><?= $user['fullName']; ?></td>
                            <td><?= $user['userName']; ?></td>
                            <td><?= $user['hakAkses']; ?></td>
                            <td class="text-center">
                                <a href="javascript:void(0)" id="e_" <?= $user['userID']; ?> class="mx-1 btn btn-primary euser"><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" id="r_" <?= $user['userID']; ?> class="mx-1 btn btn-danger ruser"><i class="fas fa-trash"></i></a>
                                <a href="javascript:void(0)" id="p_" <?= $user['userID']; ?> class="mx-1 btn btn-warning puser"><i class="fas fa-lock"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modals -->
<!-- Modal Customer -->
<div class="modal" tabindex="-1" role="dialog" id="mdUser">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengguna Sistem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>Admin/tambahUser" method="post" class="form-horizontal">
                    <input type="hidden" name="mode" id="mode" value="baru">
                    <div class="form-group row">
                        <label for="userID" class="col-sm-3">ID User</label>
                        <div class="col-sm-9">
                            <input type="text" name="userID" id="userID" class="form-control" readonly maxlength='16'>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="fullName" class="col-sm-3">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" name="fullName" id="fullName" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userName" class="col-sm-3">Nama Pengguna</label>
                        <div class="col-sm-9">
                            <input type="text" name="userName" id="userName" class="form-control" required>
                        </div>
                    </div>
                    <!-- userID , fullName , userName , userPass , hakAkses -->

                    <div class="form-group row">
                        <label for="hakAkses" class="col-sm-3">Hak Akses</label>
                        <div class="col-sm-9">
                            <select name="hakAkses" id="hakAkses" class="form-control">
                                <option value="Loket">Loket</option>
                                <option value="Hhp">HHP</option>
                                <option value="Admin">Admin</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userPass" class="col-sm-3">Password</label>
                        <div class="col-sm-9">
                            <input type="text" name="userPass" id="userPass" class="form-control" value="bpnbanjar">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Customer -->
<!-- Modal Password -->
<div class="modal" tabindex="-1" role="dialog" id="mdPassword">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengguna Sistem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>Admin/resetPassword" method="post" class="form-horizontal">
                    <input type="hidden" name="mode" id="mode" value="baru">
                    <div class="form-group row">
                        <label for="userID" class="col-sm-3">ID User</label>
                        <div class="col-sm-9">
                            <input type="text" name="userID" id="puserID" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userName" class="col-sm-3">Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="userName" id="puserName" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="userPass" class="col-sm-3">Password</label>
                        <div class="col-sm-9">
                            <input type="text" name="userPass" id="puserPass" class="form-control" value="<?= genpass(); ?>">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Password -->
<?php $this->view('template/bs4js'); ?>
<script>
    $(".euser").click(function() {
        let userID = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        let fullName = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
        let userName = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
        let hakAkses = $(this).parent('td').parent('tr').children('td:nth-child(4)').text();
        $("#mdUser").modal('show');
        $("#fullName").val(fullName);
        $("#userName").val(userName);
        $("#hakAkses").val(hakAkses);
        $("#mode").val('ubah');
        $("#userID").val(userID);
        $('form').prop('action', "<?= BASEURL; ?>Admin/ngubahUser");
    })

    $(".puser").click(function() {
        let userID = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        let userName = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
        // alert(userID);
        $('#mdPassword').modal('show');
        $('#puserID').val(userID);
        $('#puserName').val(userName);
    })

    $('.ruser').click(function() {
        let userID = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        let tenan = confirm('User akan dihapus!');
        if (tenan == true) {
            $.post('<?= BASEURL; ?>Admin/sampahUser', {
                userID: userID
            }, function(res) {
                if (res == '1') {
                    alert('User telah dihapus');
                    location.reload();
                }
            })
        }
    })
</script>