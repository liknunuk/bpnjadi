<?php
class Model_desa
{
    private $table = "desabanjarnegara";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function kodeDesa($id){
        $sql = "SELECT kodeDesa kode , kecamatan , namaDesa desa FROM $this->table WHERE namaDesa LIKE :id";
        $this->db->query($sql);
        $this->db->bind('id',"%{$id}%");
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
