<?php
class Model_pemohon
{
    private $table = "pengaju";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET golongan=:golongan , namaLengkap=:namaLengkap , alamat=:alamat , nomorTelepon=:nomorTelepon , email=:email , nikPengaju=:nikPengaju ";
        $this->db->query($sql);
        $this->db->bind('golongan', $data['golongan']);
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('nomorTelepon', $data['nomorTelepon']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('nikPengaju', $data['nikPengaju']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table  SET namaLengkap=:namaLengkap , alamat=:alamat , nomorTelepon=:nomorTelepon , email=:email WHERE nikPengaju=:nikPengaju";

        $this->db->query($sql);
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('nomorTelepon', $data['nomorTelepon']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('nikPengaju', $data['nikPengaju']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE nikPengaju=:nikPengaju";
        $this->db->query($sql);
        $this->db->bind('nikPengaju', $data['nikPengaju']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE nikPengaju=:key";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // Daftar Notaris
    public function daftarNotaris($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM pengaju WHERE golongan = 'notaris' ORDER BY namaLengkap LIMIT $row , " . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function tambahNotaris($data)
    {
        $katasandi = md5($data['nikPengaju'] . "+_+" . $_POST['katasandi']);
        $sql = "INSERT INTO $this->table SET golongan=:golongan , namaLengkap=:namaLengkap , alamat=:alamat , nomorTelepon=:nomorTelepon , email=:email , katasandi=:katasandi , nikPengaju=:nikPengaju ";
        $this->db->query($sql);
        $this->db->bind('golongan', 'Notaris');
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('alamat', $data['alamat']);
        $this->db->bind('nomorTelepon', $data['nomorTelepon']);
        $this->db->bind('email', $data['email']);
        $this->db->bind('katasandi', $katasandi);
        $this->db->bind('nikPengaju', $data['nikPengaju']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function passcha($data)
    {
        $katasandi = md5($data['nikPengaju'] . "+_+" . $_POST['katasandi']);
        $sql = "UPDATE $this->table SET katasandi=:katasandi WHERE nikPengaju=:nikPengaju LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('katasandi', $katasandi);
        $this->db->bind('nikPengaju', $data['nikPengaju']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // CUSTOMIZED QUERY //
    public function cekPemohon($data)
    {
        $sql = "SELECT COUNt(nikPengaju) nikPemohon FROM pengaju WHERE nikPengaju=:nikPemohon";
        $this->db->query($sql);
        $this->db->bind('nikPemohon', $data['nikPemohon']);
        $result = $this->db->resultOne();
        return $result['nikPemohon'];
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
