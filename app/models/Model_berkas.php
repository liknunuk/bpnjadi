<?php
class Model_berkas
{
    private $table = "berkasJadi";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $deca = $this->descamat($data['desa']);
        $sql = "INSERT INTO $this->table SET kegiatan=:kegiatan , tahunBerkas=:tahunBerkas , namaPemilik=:namaPemilik , nikPemilik=:nikPemilik , jenisHak=:jenisHak , nomorHak=:nomorHak , tanggaljadi=:tanggalJadi,userID=:userID,desa=:desa,kecamatan=:kecamatan , diajukanOleh=:diajukanOleh , nomorTanggungan=:nomorTanggungan , keterangan=:keterangan , nomorBerkas=:nomorBerkas";
        $this->db->query($sql);
        $this->db->bind('kegiatan', $data['kegiatan']);
        $this->db->bind('tahunBerkas', $data['tahunBerkas']);
        $this->db->bind('namaPemilik', $data['namaPemilik']);
        $this->db->bind('nikPemilik', $data['nikPemilik']);
        $this->db->bind('jenisHak', $data['jenisHak']);
        $this->db->bind('nomorHak', $data['nomorHak']);
        $this->db->bind('tanggalJadi', $data['tanggalJadi']);
        $this->db->bind('desa', $deca['desa']);
        $this->db->bind('kecamatan', $deca['kecamatan']);
        $this->db->bind('diajukanOleh', $data['diajukanOleh']);
        $this->db->bind('nomorTanggungan', $data['nomorTanggungan']);
        $this->db->bind('keterangan', $data['keterangan']);
        $this->db->bind('nomorBerkas', $data['nomorBerkas'] . '-' . $data['tahunBerkas']);
        $this->db->bind('userID', $_SESSION['userID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $deca = $this->descamat($data['desa']);
        $sql = "UPDATE $this->table SET kegiatan=:kegiatan , tahunBerkas=:tahunBerkas , namaPemilik=:namaPemilik , nikPemilik=:nikPemilik , jenisHak=:jenisHak , nomorHak=:nomorHak , tanggaljadi=:tanggalJadi,userID=:userID,desa=:desa,kecamatan=:kecamatan,diajukanOleh=:diajukanOleh , nomorTanggungan=:nomorTanggungan , keterangan=:keterangan  WHERE nomorBerkas=:nomorBerkas";
        $this->db->query($sql);
        $this->db->bind('kegiatan', $data['kegiatan']);
        $this->db->bind('tahunBerkas', $data['tahunBerkas']);
        $this->db->bind('namaPemilik', $data['namaPemilik']);
        $this->db->bind('nikPemilik', $data['nikPemilik']);
        $this->db->bind('jenisHak', $data['jenisHak']);
        $this->db->bind('nomorHak', $data['nomorHak']);
        $this->db->bind('tanggalJadi', $data['tanggalJadi']);
        $this->db->bind('desa', $deca['desa']);
        $this->db->bind('kecamatan', $deca['kecamatan']);
        $this->db->bind('diajukanOleh', $data['diajukanOleh']);
        $this->db->bind('nomorTanggungan', $data['nomorTanggungan']);
        $this->db->bind('keterangan', $data['keterangan']);
        $this->db->bind('nomorBerkas', $data['nomorBerkas'] . '-' . $data['tahunBerkas']);
        $this->db->bind('userID', $_SESSION['userID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE nomorBerkas=:nomorBerkas";
        $this->db->query($sql);
        $this->db->bind('nomorBerkas', $data['nomorBerkas']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampilJadi($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT diajukanOleh , kegiatan , nomorBerkas , tahunBerkas , nikPemilik, namaPemilik, jenisHak , nomorHak , tanggalJadi FROM $this->table WHERE statusBerkas='1' ORDER BY nomorBerkas DESC LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function tampilAmbil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT diajukanOleh , kegiatan , nomorBerkas , namaPemilik , jenisHak , nomorHak , diambilTanggal , nikPengaju , namaLengkap FROM $this->table , pengaju WHERE statusBerkas='0' && pengaju.nikPengaju = berkasJadi.diambilOleh ORDER BY diambilTanggal DESC LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT berkas.tahunBerkas , berkas.diajukanOleh , berkas.nomorBerkas , berkas.nikPemilik , berkas.namaPemilik, berkas.kegiatan , berkas.jenisHak , berkas.nomorHak , desa.namaDesa , desa.kecamatan , DATE_FORMAT(berkas.tanggaljadi,'%d-%m-%Y') tanggalJadi , berkas.desa , berkas.statusBerkas , DATE_FORMAT(berkas.diambilTanggal,'%d-%m-%Y') diambilTanggal , berkas.diambilOleh , berkas.suratKuasa, berkas.keterangan, berkas.nomorTanggungan  FROM berkasJadi berkas , desabanjarnegara desa WHERE nomorBerkas = :key && desa.kodedesa = berkas.desa LIMIT 1;";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    public function wilayah($kodeDesa)
    {
        $sql = "SELECT kecamatan , namaDesa FROM desabanjarnegara WHERE kodeDesa = :kodeDesa LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('kodeDesa', $kodeDesa);
        return $this->db->resultOne();
    }
    // Split Desa and Kecamatan
    private function descamat($data)
    {
        $kecamatan = substr($data, 0, 6);
        return array('desa' => $data, 'kecamatan' => $kecamatan);
    }

    // CUSTOMIZED QUERY //
    public function diambil($data)
    {

        $sql = "UPDATE $this->table SET diambilTanggal=:diambilTanggal , diambilOleh=:diambilOleh , suratKuasa=:suratKuasa, statusBerkas='0' WHERE nomorBerkas=:nomorBerkas";
        $this->db->query($sql);
        $this->db->bind('diambilTanggal', $data['diambilTanggal']);
        $this->db->bind('diambilOleh', $data['diambilOleh']);
        $this->db->bind('suratKuasa', $data['suratKuasa']);
        $this->db->bind('nomorBerkas', $data['nomorBerkas']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // sertipikat jadi bulanan
    public function jadibulan($bulan)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi LIKE :bulan ";
        $this->db->query($sql);
        $this->db->bind('bulan', "{$bulan}%");
        return $this->db->resultOne();
    }

    // sertipikat jadi tahunan
    public function jaditahun($tahun)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi LIKE :tahun ";
        $this->db->query($sql);
        $this->db->bind('tahun', "{$tahun}%");
        return $this->db->resultOne();
    }

    // sertipikat ambil bulanan
    public function ambilbulan($bulan)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi LIKE :bulan && statusBerkas='0'";
        $this->db->query($sql);
        $this->db->bind('bulan', "{$bulan}%");
        return $this->db->resultOne();
    }

    // sertipikat ambil tahunan
    public function ambiltahun($tahun)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi LIKE :tahun && statusBerkas='0'";
        $this->db->query($sql);
        $this->db->bind('tahun', "{$tahun}%");
        return $this->db->resultOne();
    }

    // sertipikat sisa bulanan
    public function sisabulan($bulan)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi LIKE :bulan && statusBerkas='1'";
        $this->db->query($sql);
        $this->db->bind('bulan', "{$bulan}%");
        return $this->db->resultOne();
    }

    // sertipikat sisa tahunan
    public function sisatahun($tahun)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi LIKE :tahun && statusBerkas='1'";
        $this->db->query($sql);
        $this->db->bind('tahun', "{$tahun}%");
        return $this->db->resultOne();
    }

    public function statistikPeriodik($m, $s)
    {
        // Data Berjas jadi bulan tertentu
        $jadi = $this->jadiPeriodik($m, $s);
        $ambil = $this->ambilPeriodik($m, $s);
        $sisa = $this->sisaPeriodik($m, $s);
        return ['jadi' => $jadi['jumlah'], 'ambil' => $ambil['jumlah'], 'sisa' => $sisa['jumlah']];
    }

    private function jadiPeriodik($m, $s)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi BETWEEN :m AND :s ";
        $this->db->query($sql);
        $this->db->bind('m', $m);
        $this->db->bind('s', $s);
        return $this->db->resultOne();
    }

    private function ambilPeriodik($m, $s)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi BETWEEN :m AND :s  && statusBerkas='0'";
        $this->db->query($sql);
        $this->db->bind('m', $m);
        $this->db->bind('s', $s);
        return $this->db->resultOne();
    }

    private function sisaPeriodik($m, $s)
    {
        $sql = "SELECT COUNT(nomorBerkas) jumlah FROM $this->table WHERE tanggaljadi BETWEEN :m AND :s  && statusBerkas='1'";
        $this->db->query($sql);
        $this->db->bind('m', $m);
        $this->db->bind('s', $s);
        return $this->db->resultOne();
    }

    public function cekStatus($nb)
    {
        $sql = "SELECT( SELECT statusBerkas FROM berkasJadi WHERE nomorBerkas = :nb ) staber , ( SELECT COUNT(nomorBerkas) FROM berkasJadi WHERE nomorBerkas=:nb ) cacah";
        $this->db->query($sql);
        $this->db->bind('nb', $nb);
        return $this->db->resultOne();
    }

    public function berkasNotaris($ppatkid)
    {
        $sql = "SELECT berkasJadi.* , namadesa, desabanjarnegara.kecamatan kec FROM berkasJadi , desabanjarnegara WHERE diajukanOleh=:ppatkid && desabanjarnegara.kodedesa = berkasJadi.desa ORDER BY tanggaljadi DESC LIMIT 100";
        $this->db->query($sql);
        $this->db->bind('ppatkid', $ppatkid);
        return $this->db->resultSet();
    }

    // pencarian dengan kunci nomor berkas, nomor hak, nomor, tanggungan
    public function cariberkas($par)
    {
        $sql = "SELECT berkasJadi.* , namadesa, desabanjarnegara.kecamatan kec FROM berkasJadi , desabanjarnegara WHERE (nomorBerkas =:par1 || nomorHak =:par1 || nomorTanggungan = :par2) && desabanjarnegara.kodedesa = berkasJadi.desa";
        $this->db->query($sql);
        $this->db->bind('par1', $par);
        $this->db->bind('par2', $par);
        $this->db->bind('par3', $par);
        return $this->db->resultOne();
    }

    public function berkasPeriodik($m, $s)
    {
        $sql = "SELECT kegiatan,nomorBerkas,namaPemilik,namadesa,desabanjarnegara.kecamatan namakecamatan,jenisHak,nomorHak,DATE_FORMAT(tanggalJadi,'%d-%m-%Y') tanggalJadi, DATE_FORMAT(diambilTanggal , '%d-%m-%Y') diambilTanggal FROM berkasJadi, desabanjarnegara WHERE ( diambilTanggal BETWEEN :mj AND :sj || tanggaljadi BETWEEN :ma AND :sa) && berkasJadi.desa = desabanjarnegara.kodeDesa ";
        $this->db->query($sql);
        $this->db->bind('mj', $m);
        $this->db->bind('ma', $m);
        $this->db->bind('sj', $s);
        $this->db->bind('sa', $s);
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
