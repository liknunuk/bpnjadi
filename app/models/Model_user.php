<?php
class Model_user
{
    private $table = "pengguna";
    // Kolome: userID , fullName , userName , userPass , hakAkses
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        // Kolome: userID , fullName , userName , userPass , hakAkses
        $userPass = $this->setKeypass($data);
        $sql = "INSERT INTO $this->table SET fullName=:fullName , userName=:userName , userPass=:userPass , hakAkses=:hakAkses , userID=NULL ";
        $this->db->query($sql);
        $this->db->bind('fullName', $data['fullName']);
        $this->db->bind('userName', $data['userName']);
        $this->db->bind('hakAkses', $data['hakAkses']);
        $this->db->bind('userPass', $userPass);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $userPass = $this->setKeypass($data);
        $sql = "UPDATE $this->table SET fullName=:fullName , userName=:userName , userPass=:userPass ,hakAkses=:hakAkses  WHERE userID=:userID";
        $this->db->query($sql);
        $this->db->bind('fullName', $data['fullName']);
        $this->db->bind('userName', $data['userName']);
        $this->db->bind('userPass', $userPass);
        $this->db->bind('hakAkses', $data['hakAkses']);
        $this->db->bind('userID', $data['userID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE userID=:userID LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('userID', $data['userID']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY fullName  LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE xxx=:key ";
        $this->db->query($sql);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    // Authentication
    public function auth($data)
    {
        $keypass = $this->setKeypass($data);
        $sql = "SELECT * FROM $this->table WHERE userPass=:keypass";
        $this->db->query($sql);
        $this->db->bind('keypass', $keypass);
        return $this->db->resultOne();
    }

    public function notarisAuth($data)
    {
        $katasandi = md5($data['username'] . '+_+' . $data['password']);
        $sql = "SELECT * FROM pengaju WHERE katasandi=:katasandi";
        $this->db->query($sql);
        $this->db->bind('katasandi', $katasandi);
        return $this->db->resultOne();
    }

    private function setKeypass($data)
    {
        return md5($data['userName'] . "<*>" . $data['userPass']);
    }

    // CUSTOMIZED QUERY //
    public function userChPass($data)
    {
        $userPass = $this->setKeypass($data);
        $sql = "UPDATE $this->table SET userPass=:userPass WHERE userID=:userID LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('userPass', $userPass);
        $this->db->bind('userID', $data['userID']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
